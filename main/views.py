from django.shortcuts import render
from django.views.generic import TemplateView

from main.controllers.NagiosData import NagiosData

import time

# Create your views here.

class Dashboard(TemplateView):
    def get(self, request, **kwargs):
        ND = NagiosData()
        hosts = ND.get_hosts()
        services = ND.get_services()

        host_summary = {
            'down': 0,
            'up': 0,
            'unreachable': 0,
            'pending': 0
        }
        for host in hosts.values():
            if host["state"] == 0:
                host_summary["up"] += 1
            elif host["state"] == 1:
                host_summary["down"] += 1
            elif host["state"] == 2:
                host_summary["unreachable"] += 1
            else:
                host_summary["pending"] += 1
        
        num_hosts = len(hosts)
        host_summary_percent = {
            'down': round((host_summary['down'] / num_hosts)*100),
            'up': round((host_summary['up'] / num_hosts)*100),
            'unreachable': round((host_summary['unreachable'] / num_hosts)*100),
            'pending': round((host_summary['pending'] / num_hosts)*100)
        }

        num_services = len(services)
        service_summary = {
            'critical': 0,
            'ok': 0,
            'warning': 0,
            'pending': 0
        }
        for service in services:
            if service["state"] == 0:
                service_summary["ok"] += 1
            elif service["state"] == 1:
                service_summary["warning"] += 1
            elif service["state"] == 2:
                service_summary["critical"] += 1
            elif service["state"] == 3:
                service_summary["unknown"] += 1
            else:
                service_summary["pending"] += 1
        service_summary_percent = {
            'critical': round((service_summary['critical'] / num_services)*100),
            'ok': round((service_summary['ok'] / num_services)*100),
            'warning': round((service_summary['warning'] / num_services)*100),
            'pending': round((service_summary['pending'] / num_services)*100)
        }

        epoch_time = int(time.time())
        pnp_graph_end = epoch_time
        pnp_graph_start = epoch_time - (1*60*24)  # 24 hours

        return render(request, 'index.html', {'hosts': hosts,
            'host_summary': host_summary,
            'host_summary_percent': host_summary_percent,
            'services': services, 'service_summary': service_summary,
            'service_summary_percent': service_summary_percent,
            'pnp_graph_start': pnp_graph_start,
            'pnp_graph_end': pnp_graph_end})


class Hosts(TemplateView):
    def get(self, request, **kwargs):
        ND = NagiosData()
        hosts = ND.get_hosts()

        host_summary = {
            'down': 0,
            'up': 0,
            'unreachable': 0,
            'pending': 0
        }
        for host in hosts.values():
            if host["state"] == 0:
                host_summary["up"] += 1
            elif host["state"] == 1:
                host_summary["down"] += 1
            elif host["state"] == 2:
                host_summary["unreachable"] += 1
            else:
                host_summary["pending"] += 1

        return render(request, 'hosts.html', {'hosts': hosts, 'host_summary': host_summary})


class Services(TemplateView):
    def get(self, request, **kwargs):
        ND = NagiosData()
        services = ND.get_services()
        
        service_summary = {
            'critical': 0,
            'ok': 0,
            'warning': 0,
            'pending': 0
        }
        for service in services:
            if service["state"] == 0:
                service_summary["ok"] += 1
            elif service["state"] == 1:
                service_summary["warning"] += 1
            elif service["state"] == 2:
                service_summary["critical"] += 1
            elif service["state"] == 3:
                service_summary["unknown"] += 1
            else:
                service_summary["pending"] += 1

        epoch_time = int(time.time())
        pnp_graph_end = epoch_time
        pnp_graph_start = epoch_time - (1*60*60*24)  # 24 hours
        return render(request, 'services.html', {'services': services,
            'service_summary': service_summary,
            'pnp_graph_start': pnp_graph_start,
            'pnp_graph_end': pnp_graph_end })
