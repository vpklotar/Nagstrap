import socket
import time
import pprint
import json

from main.controllers.Config import Config

class NagiosData:

    pp = pprint.PrettyPrinter(depth=4)

    def __init__(self):
          self.config = Config()
          self.socket_path = self.config.get("nagios.livestatus_socket_location")

    def send_get_command(self, command):
        s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        s.connect(self.socket_path)
        s.send(bytes("%s%s" % (command, "\nOutputFormat: json\n"), "UTF-8"))
        s.shutdown(socket.SHUT_WR)
        answer = s.recv(100000000).decode('UTF-8')

        data = json.loads(answer)

        return data
    
    def get_hosts(self):
        data = self.send_get_command("GET hosts")
        hosts = {}
        for host in data[1:]:
            phost = self.parse_host(host)
            hosts[phost["name"]] = phost
        return hosts
    
    def get_services(self):
        data = self.send_get_command("GET services")
        services = []
        for service in data[1:]:
            pservice = self.parse_service(service)
            services.append(pservice)
        return services
    
    def parse_service(self, data):
        service_dict = {}
        service_dict["accept_passive_checks"] = data[0]
        service_dict["acknowledged"] = data[1]
        service_dict["acknowledgement_type"] = data[2]
        service_dict["action_url"] = data[3]
        service_dict["action_url_expanded"] = data[4]
        service_dict["active_checks_enabled"] = data[5]
        service_dict["cache_interval"] = data[6]
        service_dict["cached_at"] = data[7]
        service_dict["check_command"] = data[8]
        service_dict["check_command_expanded"] = data[9]
        service_dict["check_freshness"] = data[10]
        service_dict["check_interval"] = data[11]
        service_dict["check_options"] = data[12]
        service_dict["check_period"] = data[13]
        service_dict["check_type"] = data[14]
        service_dict["checks_enabled"] = data[15]
        service_dict["comments"] = data[16]
        service_dict["comments_with_extra_info"] = data[17]
        service_dict["comments_with_info"] = data[18]
        service_dict["contact_groups"] = data[19]
        service_dict["contacts"] = data[20]
        service_dict["current_attempt"] = data[21]
        service_dict["current_notification_number"] = data[22]
        service_dict["custom_variable_names"] = data[23]
        service_dict["custom_variable_values"] = data[24]
        service_dict["custom_variables"] = data[25]
        service_dict["description"] = data[26]
        service_dict["display_name"] = data[27]
        service_dict["downtimes"] = data[28]
        service_dict["downtimes_with_info"] = data[29]
        service_dict["event_handler"] = data[30]
        service_dict["event_handler_enabled"] = data[31]
        service_dict["execution_time"] = data[32]
        service_dict["first_notification_delay"] = data[33]
        service_dict["flap_detection_enabled"] = data[34]
        service_dict["groups"] = data[35]
        service_dict["has_been_checked"] = data[36]
        service_dict["high_flap_threshold"] = data[37]
        service_dict["host_accept_passive_checks"] = data[38]
        service_dict["host_acknowledged"] = data[39]
        service_dict["host_acknowledgement_type"] = data[40]
        service_dict["host_action_url"] = data[41]
        service_dict["host_action_url_expanded"] = data[42]
        service_dict["host_active_checks_enabled"] = data[43]
        service_dict["host_address"] = data[44]
        service_dict["host_alias"] = data[45]
        service_dict["host_check_command"] = data[46]
        service_dict["host_check_command_expanded"] = data[47]
        service_dict["host_check_flapping_recovery_notification"] = data[48]
        service_dict["host_check_freshness"] = data[49]
        service_dict["host_check_interval"] = data[50]
        service_dict["host_check_options"] = data[51]
        service_dict["host_check_period"] = data[52]
        service_dict["host_check_type"] = data[53]
        service_dict["host_checks_enabled"] = data[54]
        service_dict["host_childs"] = data[55]
        service_dict["host_comments"] = data[56]
        service_dict["host_comments_with_extra_info"] = data[57]
        service_dict["host_comments_with_info"] = data[58]
        service_dict["host_contact_groups"] = data[59]
        service_dict["host_contacts"] = data[60]
        service_dict["host_current_attempt"] = data[61]
        service_dict["host_current_notification_number"] = data[62]
        service_dict["host_custom_variable_names"] = data[63]
        service_dict["host_custom_variable_values"] = data[64]
        service_dict["host_custom_variables"] = data[65]
        service_dict["host_display_name"] = data[66]
        service_dict["host_downtimes"] = data[67]
        service_dict["host_downtimes_with_info"] = data[68]
        service_dict["host_event_handler"] = data[69]
        service_dict["host_event_handler_enabled"] = data[70]
        service_dict["host_execution_time"] = data[71]
        service_dict["host_filename"] = data[72]
        service_dict["host_first_notification_delay"] = data[73]
        service_dict["host_flap_detection_enabled"] = data[74]
        service_dict["host_groups"] = data[75]
        service_dict["host_hard_state"] = data[76]
        service_dict["host_has_been_checked"] = data[77]
        service_dict["host_high_flap_threshold"] = data[78]
        service_dict["host_icon_image"] = data[79]
        service_dict["host_icon_image_alt"] = data[80]
        service_dict["host_icon_image_expanded"] = data[81]
        service_dict["host_in_check_period"] = data[82]
        service_dict["host_in_notification_period"] = data[83]
        service_dict["host_in_service_period"] = data[84]
        service_dict["host_initial_state"] = data[85]
        service_dict["host_is_executing"] = data[86]
        service_dict["host_is_flapping"] = data[87]
        service_dict["host_last_check"] = data[88]
        service_dict["host_last_hard_state"] = data[89]
        service_dict["host_last_hard_state_change"] = data[90]
        service_dict["host_last_notification"] = data[91]
        service_dict["host_last_state"] = data[92]
        service_dict["host_last_state_change"] = data[93]
        service_dict["host_last_time_down"] = data[94]
        service_dict["host_last_time_unreachable"] = data[95]
        service_dict["host_last_time_up"] = data[96]
        service_dict["host_latency"] = data[97]
        service_dict["host_long_plugin_output"] = data[98]
        service_dict["host_low_flap_threshold"] = data[99]
        service_dict["host_max_check_attempts"] = data[100]
        service_dict["host_metrics"] = data[101]
        service_dict["host_mk_inventory"] = data[102]
        service_dict["host_mk_inventory_gz"] = data[103]
        service_dict["host_mk_inventory_last"] = data[104]
        service_dict["host_mk_logwatch_files"] = data[105]
        service_dict["host_modified_attributes"] = data[106]
        service_dict["host_modified_attributes_list"] = data[107]
        service_dict["host_name"] = data[108]
        service_dict["host_next_check"] = data[109]
        service_dict["host_next_notification"] = data[110]
        service_dict["host_no_more_notifications"] = data[111]
        service_dict["host_notes"] = data[112]
        service_dict["host_notes_expanded"] = data[113]
        service_dict["host_notes_url"] = data[114]
        service_dict["host_notes_url_expanded"] = data[115]
        service_dict["host_notification_interval"] = data[116]
        service_dict["host_notification_period"] = data[117]
        service_dict["host_notifications_enabled"] = data[118]
        service_dict["host_num_services"] = data[119]
        service_dict["host_num_services_crit"] = data[120]
        service_dict["host_num_services_hard_crit"] = data[121]
        service_dict["host_num_services_hard_ok"] = data[122]
        service_dict["host_num_services_hard_unknown"] = data[123]
        service_dict["host_num_services_hard_warn"] = data[124]
        service_dict["host_num_services_ok"] = data[125]
        service_dict["host_num_services_pending"] = data[126]
        service_dict["host_num_services_unknown"] = data[127]
        service_dict["host_num_services_warn"] = data[128]
        service_dict["host_obsess_over_host"] = data[129]
        service_dict["host_parents"] = data[130]
        service_dict["host_pending_flex_downtime"] = data[131]
        service_dict["host_percent_state_change"] = data[132]
        service_dict["host_perf_data"] = data[133]
        service_dict["host_plugin_output"] = data[134]
        service_dict["host_pnpgraph_present"] = data[135]
        service_dict["host_process_performance_data"] = data[136]
        service_dict["host_retry_interval"] = data[137]
        service_dict["host_scheduled_downtime_depth"] = data[138]
        service_dict["host_service_period"] = data[139]
        service_dict["host_services"] = data[140]
        service_dict["host_services_with_fullstate"] = data[141]
        service_dict["host_services_with_info"] = data[142]
        service_dict["host_services_with_state"] = data[143]
        service_dict["host_staleness"] = data[144]
        service_dict["host_state"] = data[145]
        service_dict["host_state_type"] = data[146]
        service_dict["host_statusmap_image"] = data[147]
        service_dict["host_total_services"] = data[148]
        service_dict["host_worst_service_hard_state"] = data[149]
        service_dict["host_worst_service_state"] = data[150]
        service_dict["host_x_3d"] = data[151]
        service_dict["host_y_3d"] = data[152]
        service_dict["host_z_3d"] = data[153]
        service_dict["icon_image"] = data[154]
        service_dict["icon_image_alt"] = data[155]
        service_dict["icon_image_expanded"] = data[156]
        service_dict["in_check_period"] = data[157]
        service_dict["in_notification_period"] = data[158]
        service_dict["in_service_period"] = data[159]
        service_dict["initial_state"] = data[160]
        service_dict["is_executing"] = data[161]
        service_dict["is_flapping"] = data[162]
        service_dict["last_check"] = data[163]
        service_dict["last_hard_state"] = data[164]
        service_dict["last_hard_state_change"] = data[165]
        service_dict["last_notification"] = data[166]
        service_dict["last_state"] = data[167]
        service_dict["last_state_change"] = data[168]
        service_dict["last_time_critical"] = data[169]
        service_dict["last_time_ok"] = data[170]
        service_dict["last_time_unknown"] = data[171]
        service_dict["last_time_warning"] = data[172]
        service_dict["latency"] = data[173]
        service_dict["long_plugin_output"] = data[174]
        service_dict["low_flap_threshold"] = data[175]
        service_dict["max_check_attempts"] = data[176]
        service_dict["metrics"] = data[177]
        service_dict["modified_attributes"] = data[178]
        service_dict["modified_attributes_list"] = data[179]
        service_dict["next_check"] = data[180]
        service_dict["next_notification"] = data[181]
        service_dict["no_more_notifications"] = data[182]
        service_dict["notes"] = data[183]
        service_dict["notes_expanded"] = data[184]
        service_dict["notes_url"] = data[185]
        service_dict["notes_url_expanded"] = data[186]
        service_dict["notification_interval"] = data[187]
        service_dict["notification_period"] = data[188]
        service_dict["notifications_enabled"] = data[189]
        service_dict["obsess_over_service"] = data[190]
        service_dict["percent_state_change"] = data[191]
        service_dict["perf_data"] = data[192]
        service_dict["plugin_output"] = data[193]
        service_dict["pnpgraph_present"] = data[194]
        service_dict["process_performance_data"] = data[195]
        service_dict["retry_interval"] = data[196]
        service_dict["scheduled_downtime_depth"] = data[197]
        service_dict["service_period"] = data[198]
        service_dict["staleness"] = data[199]
        service_dict["state"] = data[200]
        service_dict["state_type"] = data[201]

        epoc_time = int(time.time())
        service_dict["last_check_human"] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(service_dict["last_check"])))
        service_dict["last_state_change_human"] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(service_dict["last_state_change"])))
        service_dict["duration"] = epoc_time - int(service_dict["last_state_change"])
        service_dict["duration_human"] = time.strftime('%-dd %Hh %Mm %Ss', time.localtime(int(service_dict["duration"])))

        if service_dict["state"] == 0:
            service_dict["state_string"] = "OK"
            service_dict["service_background_class"] = "table-success"
            service_dict["service_background_line_class"] = ""
        elif service_dict["state"] == 1:
            service_dict["state_string"] = "WARNING"
            service_dict["service_background_class"] = "table-warning"
            service_dict["service_background_line_class"] = "table-warning"
        elif service_dict["state"] == 2:
            service_dict["state_string"] = "CRITICAL"
            service_dict["service_background_class"] = "table-danger"
            service_dict["service_background_line_class"] = "table-danger"
        elif service_dict["state"] == 3:
            service_dict["state_string"] = "UNKNOWN"
            service_dict["service_background_class"] = "table-dark"
            service_dict["service_background_line_class"] = "table-dark"
        else:
            service_dict["state_string"] = "PENDING"
            service_dict["service_background_class"] = "table-secondary"
            service_dict["service_background_line_class"] = "table-secondary"
        
        pnp_variable = self.config.get("pnp4nagios.variable_indicator")[1:].upper()  # Get and convert indicator to mk-livestatus compatible
        service_dict["pnp_enabled"] = service_dict["custom_variables"].get(pnp_variable, 0)

        return service_dict
    
    def parse_host(self, data):
        host_dict = {}
        host_dict["accept_passive_checks"] = data[0]
        host_dict["acknowledged"] = data[1]
        host_dict["acknowledgement_type"] = data[2]
        host_dict["action_url"] = data[3]
        host_dict["action_url_expanded"] = data[4]
        host_dict["active_checks_enabled"] = data[5]
        host_dict["address"] = data[6]
        host_dict["alias"] = data[7]
        host_dict["check_command"] = data[8]
        host_dict["check_command_expanded"] = data[9]
        host_dict["check_flapping_recovery_notification"] = data[10]
        host_dict["check_freshness"] = data[11]
        host_dict["check_interval"] = data[12]
        host_dict["check_options"] = data[13]
        host_dict["check_period"] = data[14]
        host_dict["check_type"] = data[15]
        host_dict["checks_enabled"] = data[16]
        host_dict["childs"] = data[17]
        host_dict["comments"] = data[18]
        host_dict["comments_with_extra_info"] = data[19]
        host_dict["comments_with_info"] = data[20]
        host_dict["contact_groups"] = data[21]
        host_dict["contacts"] = data[22]
        host_dict["current_attempt"] = data[23]
        host_dict["current_notification_number"] = data[24]
        host_dict["custom_variable_names"] = data[25]
        host_dict["custom_variable_values"] = data[26]
        host_dict["custom_variables"] = data[27]
        host_dict["display_name"] = data[28]
        host_dict["downtimes"] = data[29]
        host_dict["downtimes_with_info"] = data[30]
        host_dict["event_handler"] = data[31]
        host_dict["event_handler_enabled"] = data[32]
        host_dict["execution_time"] = data[33]
        host_dict["filename"] = data[34]
        host_dict["first_notification_delay"] = data[35]
        host_dict["flap_detection_enabled"] = data[36]
        host_dict["groups"] = data[37]
        host_dict["hard_state"] = data[38]
        host_dict["has_been_checked"] = data[39]
        host_dict["high_flap_threshold"] = data[40]
        host_dict["icon_image"] = data[41]
        host_dict["icon_image_alt"] = data[42]
        host_dict["icon_image_expanded"] = data[43]
        host_dict["in_check_period"] = data[44]
        host_dict["in_notification_period"] = data[45]
        host_dict["in_service_period"] = data[46]
        host_dict["initial_state"] = data[47]
        host_dict["is_executing"] = data[48]
        host_dict["is_flapping"] = data[49]
        host_dict["last_check"] = data[50]
        host_dict["last_hard_state"] = data[51]
        host_dict["last_hard_state_change"] = data[52]
        host_dict["last_notification"] = data[53]
        host_dict["last_state"] = data[54]
        host_dict["last_state_change"] = data[55]
        host_dict["last_time_down"] = data[56]
        host_dict["last_time_unreachable"] = data[57]
        host_dict["last_time_up"] = data[58]
        host_dict["latency"] = data[59]
        host_dict["long_plugin_output"] = data[60]
        host_dict["low_flap_threshold"] = data[61]
        host_dict["max_check_attempts"] = data[62]
        host_dict["metrics"] = data[63]
        host_dict["mk_inventory"] = data[64]
        host_dict["mk_inventory_gz"] = data[65]
        host_dict["mk_inventory_last"] = data[66]
        host_dict["mk_logwatch_files"] = data[67]
        host_dict["modified_attributes"] = data[68]
        host_dict["modified_attributes_list"] = data[69]
        host_dict["name"] = data[70]
        host_dict["next_check"] = data[71]
        host_dict["next_notification"] = data[72]
        host_dict["no_more_notifications"] = data[73]
        host_dict["notes"] = data[74]
        host_dict["notes_expanded"] = data[75]
        host_dict["notes_url"] = data[76]
        host_dict["notes_url_expanded"] = data[77]
        host_dict["notification_interval"] = data[78]
        host_dict["notification_period"] = data[79]
        host_dict["notifications_enabled"] = data[80]
        host_dict["num_services"] = data[81]
        host_dict["num_services_crit"] = data[82]
        host_dict["num_services_hard_crit"] = data[83]
        host_dict["num_services_hard_ok"] = data[84]
        host_dict["num_services_hard_unknown"] = data[85]
        host_dict["num_services_hard_warn"] = data[86]
        host_dict["num_services_ok"] = data[87]
        host_dict["num_services_pending"] = data[88]
        host_dict["num_services_unknown"] = data[89]
        host_dict["num_services_warn"] = data[90]
        host_dict["obsess_over_host"] = data[91]
        host_dict["parents"] = data[92]
        host_dict["pending_flex_downtime"] = data[93]
        host_dict["percent_state_change"] = data[94]
        host_dict["perf_data"] = data[95]
        host_dict["plugin_output"] = data[96]
        host_dict["pnpgraph_present"] = data[97]
        host_dict["process_performance_data"] = data[98]
        host_dict["retry_interval"] = data[99]
        host_dict["scheduled_downtime_depth"] = data[100]
        host_dict["service_period"] = data[101]
        host_dict["services"] = data[102]
        host_dict["services_with_fullstate"] = data[103]
        host_dict["services_with_info"] = data[104]
        host_dict["services_with_state"] = data[105]
        host_dict["staleness"] = data[106]
        host_dict["state"] = data[107]
        host_dict["state_type"] = data[108]
        host_dict["statusmap_image"] = data[109]
        host_dict["total_services"] = data[110]
        host_dict["worst_service_hard_state"] = data[111]
        host_dict["worst_service_state"] = data[112]
        host_dict["x_3d"] = data[113]
        host_dict["y_3d"] = data[114]
        host_dict["z_3d"] = data[115]

        if host_dict["state"] == 0:
            host_dict["state_string"] = "UP"
            host_dict["host_background_class"] = "table-success" 
        elif host_dict["state"] == 1:
            host_dict["state_string"] = "DOWN"
            host_dict["host_background_class"] = "table-danger" 
        elif host_dict["state"] == 2:
            host_dict["state_string"] = "UNREACHABLE"
            host_dict["host_background_class"] = "table-dark" 
        else:
            host_dict["state_string"] = "PENDING"
            host_dict["host_background_class"] = "table-secondary" 
        
        epoc_time = int(time.time())
        host_dict["last_check_human"] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(host_dict["last_check"])))
        host_dict["last_state_change_human"] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(host_dict["last_state_change"])))
        host_dict["duration"] = epoc_time - int(host_dict["last_state_change"])
        host_dict["duration_human"] = time.strftime('%-dd %Hh %Mm %Ss', time.localtime(int(host_dict["duration"])))

        return host_dict