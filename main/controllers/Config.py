import json

class Config:

    config_location = "main/config/nagstrap.json"

    def __init__(self):
        with open(self.config_location, 'r') as f:
            config_str = f.read()
        
        self.config = json.loads(config_str)

    # Convert, example nagios.livestatus_socket_location into a set of levels in the json file and traverse them.    
    def get(self, path):
        split_path = path.split('.')
       
        current_object = self.config  # The master JSON object

        for part in split_path[:1]:
            if part in current_object:
                current_object = current_object[part]

        return current_object.get(split_path[-1].strip())